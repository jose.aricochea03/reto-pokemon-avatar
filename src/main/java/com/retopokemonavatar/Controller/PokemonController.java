package com.retopokemonavatar.Controller;

import com.retopokemonavatar.Model.Pokemon;
import com.retopokemonavatar.Services.PokemonServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.*;

@Controller
@RequestMapping("/pokemon")
public class PokemonController {

    @Autowired
    private PokemonServices pokemonService;

    @GetMapping("/name/{name}")
    public Mono<Pokemon> getPokemonByName(@PathVariable String name){
        return pokemonService.getPokemonByName(name);
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("pokemonList", pokemonService.getAllWithDetails().collectList().block());
        System.out.println(model);
        return "list";
    }

    @GetMapping("/details/{id}")
    public String getDetails(@PathVariable int id, Model model){
        Pokemon pokemon = pokemonService.getPokemonById(id).block();

        int statsTotal = pokemon.getStats().stream()
                .mapToInt(stat -> stat.getBase_stat())
                .sum();

        pokemon.setStatsTotal(statsTotal);

        model.addAttribute("pokemon", pokemon);
        System.out.println(id);
        System.out.println(model);
        return "pokemonDetails";
    }

}
