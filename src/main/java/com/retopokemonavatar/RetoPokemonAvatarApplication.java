package com.retopokemonavatar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetoPokemonAvatarApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetoPokemonAvatarApplication.class, args);
    }

}
