package com.retopokemonavatar.Services;

import com.retopokemonavatar.Model.Pokemon;
import com.retopokemonavatar.Model.PokemonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;

@Service
public class PokemonServices {

    private final WebClient webClient;

    @Autowired
    public PokemonServices(WebClient.Builder webClientBuilder){
        this.webClient = webClientBuilder.baseUrl("https://pokeapi.co/api/v2").build();
    }

    public Mono<Pokemon> getPokemonById(int id) {
        return webClient
                .get()
                .uri("/pokemon/{id}", id)
                .retrieve()
                .bodyToMono(Pokemon.class);
    }

    public Mono<Pokemon> getPokemonByName(String name){
        return webClient
                .get()
                .uri("/pokemon-species/{name}", name)
                .retrieve()
                .bodyToMono(Pokemon.class);
    }

    public Flux<Pokemon> getAll() {
        return webClient.get()
                .uri("/pokemon-species")
                .retrieve()
                .bodyToMono(PokemonResponse.class)
                .flatMapMany(response -> Flux.fromIterable(response.getResults()));
    }

    public Flux<Pokemon> getAllWithDetails() {
        return getAll()
                .flatMap(pokemon -> getPokemonByName(pokemon.getName())
                        .map(details -> new Pokemon(details.getId(), details.getName(), details.getUrl()))
                ).collectSortedList(Comparator.comparingLong(Pokemon::getId)).flatMapMany(Flux::fromIterable);
    }

}
