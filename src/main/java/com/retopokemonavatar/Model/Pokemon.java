package com.retopokemonavatar.Model;

import lombok.Data;

import java.util.List;

@Data
public class Pokemon {

    private Long id;
    private String name;
    private String url;
    private int height;
    private int weight;
    private List<Ability> abilities;
    private List<Type> types;
    private List<Stat> stats;
    private int statsTotal;

    public Pokemon() {
    }

    public Pokemon(Long id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
