package com.retopokemonavatar.Model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PokemonResponse {

    private List<Pokemon> results;

    public void setResults(List<Pokemon> results) {
        this.results = results;
    }
/* @JsonCreator
    public PokemonResponse(@JsonProperty("results") List<Pokemon> results) {
        this.results = results;
    }*/

    public List<Pokemon> getResults() {
        return results;
    }

}
