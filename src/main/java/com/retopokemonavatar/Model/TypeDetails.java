package com.retopokemonavatar.Model;

import lombok.Data;

@Data
public class TypeDetails {

    private String name;

}
